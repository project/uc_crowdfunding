<?php
/**
 * @file
 * Ubercart Crowdfunding Feature module.
 *
 * Defines a product feature to turn any product into a
 * crowdfunding style donation product.
 *
 * By Arlina E. Rhoton ("Arlina", http://drupal.org/user/1055344)
 */

/**
 * The options that determine whether a node is for sale.
 *
 * @see _uc_crowdfunding_explanations()
 */
define('UC_CROWDFUNDING_TARGET_DATELINE', 1);
define('UC_CROWDFUNDING_CHECK_DATELINE', 2);

/**
 * Implements hook_form_alter().
 *
 * Alters:
 * * uc_product_feature_add_form
 * * uc_product_add_to_cart_form_*
 * * uc_product_class_form
 *
 * Summary of alterations:
 * 1) Alters the product feature add form to restrict multiple crowdfunding
 *    features from being added to a single product.
 * 2) Hide the appropriate Qty. fields on the cart view form.
 * 3) Disable the "Add to cart" form if goal has been reached or is past
 *    dateline.
 * 4) Alter the product class form to set default values.
 */
function uc_crowdfunding_form_alter(&$form, &$form_state, $form_id) {
  $nid = $form['node']['#value']->nid;

  $query = "SELECT COUNT(*) FROM {uc_product_features}
            WHERE nid = %d AND fid = '%s'";

  $node_has_cf_feature = db_result(db_query($query, $nid, 'crowdfunding'));

  // 1) Alter the product feature add form.
  if ($form_id == 'uc_product_feature_add_form') {
    // If a crowdfunding feature has already been added to this product.
    if ($node_has_cf_feature) {
      // Remove crowdfunding from the available list of features to add.
      unset($form['feature']['#options']['crowdfunding']);
    }
  }

  if (strpos($form_id, 'uc_product_add_to_cart_form_') === 0) {

    // 2) Hide the appropriate Qty. field on the cart view form.
    if ($node_has_cf_feature) {
      if (isset($form['qty'])) {
        $form['qty'] = array(
          '#type'  => 'hidden',
          '#value' => 1,
        );
      }
    }

    // 3) Disable the add to cart form if goal has been reached
    // or is past dateline.
    if ($node_has_cf_feature && !_uc_crowdfunding_is_enabled($nid)) {
      unset($form['submit']);
      if (isset($form['attributes'])) {
        unset($form['attributes']);
      }
      if (isset($form['qty'])) {
        unset($form['qty']);
      }
    }

  }

  // 4) Alter the product class form to set default values.
  if ($form_id == 'uc_product_class_form') {
    $data = variable_get('uc_crowdfunding_class_' . $form['pcid']['#value'], array());

    $data = empty($data) ? FALSE : (object) unserialize($data);

    $form['crowdfunding'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Crowdfunding product feature'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 5,
    );
    $form['crowdfunding']['default_crowdfunding'] = array(
      '#type' => 'checkbox',
      '#title' => t('Check this box to add a default product feature to every product of this class using these settings.'),
      '#default_value' => $data === FALSE ? FALSE : TRUE,
    );
    $form['crowdfunding'] += _uc_crowdfunding_feature_form($data);
    $form_crowdfunding = &$form['crowdfunding']['crowdfunding'];
    $form_crowdfunding['cf_dateline']['#type'] = 'textfield';
    $form_crowdfunding['cf_dateline']['#description'] = t('Default dateline for this product');
    $form_crowdfunding['cf_dateline']['#default_value'] = variable_get('uc_crowdfunding_global_default_dateline', '+14 days');
    if ($data) {
      $form_crowdfunding['cf_dateline']['#default_value'] = $data->cf_dateline;
      $form_crowdfunding['cf_target']['#default_value'] = $data->cf_target;
      $form_crowdfunding['cf_type']['#default_value'] = $data->cf_type;
    }
    $form['#validate'][] = 'uc_crowdfunding_feature_form_validate';
    $form['#submit'][] = 'uc_crowdfunding_product_class_submit';

    $form['submit']['#weight'] = 10;
  }
}

/**
 * Form submit handler for uc_product_class_form.
 */
function uc_crowdfunding_product_class_submit($form, &$form_state) {
  if ($form_state['values']['default_crowdfunding']) {
    $data = array(
      'cf_dateline' => $form_state['values']['cf_dateline'],
      'cf_target'   => $form_state['values']['cf_target'],
      'cf_type'     => $form_state['values']['cf_type'],
    );
    variable_set('uc_crowdfunding_class_' . $form_state['values']['pcid'], serialize($data));
  }
  else {
    variable_del('uc_crowdfunding_class_' . $form_state['values']['pcid']);
  }
}

/**
 * Implements hook_nodeapi().
 *
 * Summary of alterations:
 * 1) Removes price displays from variable priced product nodes.
 * 2) Inserts crowdfunding product feature on product node creation.
 */
function uc_crowdfunding_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if (!uc_product_is_product($node)) {
    return;
  }

  switch ($op) {
    case 'load':
      $report    = uc_crowdfunding_product_report($node->nid);
      $feature   = uc_crowdfunding_product_load($node->nid);
      $node->uc_crowdfunding_sold     = $report->sold;
      $node->uc_crowdfunding_gross    = $report->gross;
      $node->uc_crowdfunding_revenue  = $report->revenue;

      $query = "SELECT pfid FROM {uc_product_features}
                WHERE fid = 'crowdfunding' AND nid = %d";
      if (db_result(db_query($query, $node->nid))) {
        $node->uc_crowdfunding_target   = $feature->target;
        $node->uc_crowdfunding_dateline = $feature->dateline;
        $node->uc_crowdfunding_type     = $feature->type;
        $node->uc_crowdfunding_enabled  = _uc_crowdfunding_is_enabled($node->nid);
        $node->uc_crowdfunding_days_remaining = _uc_crowdfunding_days_remaing($feature->dateline);
        $node->uc_crowdfunding_goal_achieved  = _uc_crowdfunding_goal_achieved($node->nid, $feature, $report);
      }
      break;

    case 'insert':
      $data = variable_get('uc_crowdfunding_class_' . $node->type, array());

      // If product class has a default crowdfunding product feature.
      if ($data) {
        // Prepare data as if it were from a form submission.
        $data = unserialize($data);
        $data['nid'] = $node->nid;
        $data['pfid'] = '';
        $data['cf_dateline'] = strtotime($data['cf_dateline']);
        $data['cf_dateline'] = format_date($data['cf_dateline'], 'custom', 'Y-m-d H:i');
        $form_state = array('values' => $data);

        // Add feature to the product by spoofing the normal form submission.
        uc_crowdfunding_feature_form_submit(array(), $form_state);
      }
      break;

    case 'delete':
      $data = uc_crowdfunding_product_load($node->nid);
      if ($data) {
        $query = 'DELETE FROM {uc_crowdfunding_products} WHERE cfid = %d';
        db_query($query, $data->cfid);
      }
      break;
  }
}

/**
 * Implements hook_product_feature().
 */
function uc_crowdfunding_product_feature() {
  $features = array();

  $features[] = array(
    'id' => 'crowdfunding',
    'title' => t('Crowdfunding'),
    'callback' => 'uc_crowdfunding_feature_form',
    'delete' => 'uc_crowdfunding_feature_delete',
    'settings' => '_uc_crowdfunding_settings',
    'multiple' => FALSE,
  );

  return $features;
}

/**
 * Add settings to the crowdfunding product feature form.
 *
 * @return array
 *   The crowdfunding product feature form.
 */
function _uc_crowdfunding_settings() {
  $description1 = t('The global default dateline for crowdfunding products.');
  $description2 = t('Must be a format recognized by PHP strtotime.');

  $form = array();
  $form['uc_crowdfunding_global_default_dateline'] = array(
    '#title' => t('Global default dateline'),
    '#type' => 'textfield',
    '#description' => $description1 . ' ' . $description2,
    '#default_value' => variable_get('uc_crowdfunding_global_default_dateline', '+14 days'),
  );

  return $form;
}

/**
 * Build settings form for the individual crowdfunding product feature.
 */
function uc_crowdfunding_feature_form($form_state, $node, $feature) {
  $form = array();

  // Load the crowdfunding data specific to this product.
  $query = 'SELECT * FROM {uc_crowdfunding_products} WHERE pfid = %d';
  $data = db_fetch_object(db_query($query, $feature['pfid']));
  if ($data && $data->dateline) {
    $data->dateline = format_date($data->dateline, 'custom', 'Y-m-d H:i');
  }

  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['pfid'] = array(
    '#type' => 'value',
    '#value' => $data ? $data->pfid : '',
  );

  $form += _uc_crowdfunding_feature_form($data);

  return uc_product_feature_form($form);
}

/**
 * Build the crowdfunding feature form.
 *
 * @param object $data
 *   Saved crowdfunding feature data for a given node.
 */
function _uc_crowdfunding_feature_form($data = FALSE) {
  $form = array();
  $default_date = strtotime(variable_get('uc_crowdfunding_global_default_dateline', '+14days'));
  $default_date = format_date($default_date, 'custom', 'Y-m-d H:i');
  $uc_sign_after_amount = variable_get('uc_sign_after_amount', FALSE);
  $uc_currency_sign = variable_get('uc_currency_sign', '$');

  $form['#validate'] = array('uc_crowdfunding_feature_form_validate');

  $form['crowdfunding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crowdfunding settings'),
  );
  $form['crowdfunding']['cf_dateline'] = array(
    '#type' => 'date_select',
    '#title' => t('Dateline'),
    '#description' => t('The dateline for this product'),
    '#default_value' => empty($data->dateline) ? $default_date : $data->dateline,
    '#date_format' => 'Y-m-d H:i',
    '#date_label_position' => 'within',
    '#date_increment' => 15,
    '#required' => TRUE,
  );
  $form['crowdfunding']['cf_target'] = array(
    '#type' => 'textfield',
    '#title' => t('Target amount to raise'),
    '#default_value' => $data ? $data->target : 0,
    '#field_prefix' => $uc_sign_after_amount ? '' : $uc_currency_sign,
    '#field_suffix' => $uc_sign_after_amount ? $uc_currency_sign : '',
    '#required' => TRUE,
  );
  $form['crowdfunding']['cf_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#options' => array(
      UC_CROWDFUNDING_TARGET_DATELINE => _uc_crowdfunding_explanations(UC_CROWDFUNDING_TARGET_DATELINE),
      UC_CROWDFUNDING_CHECK_DATELINE  => _uc_crowdfunding_explanations(UC_CROWDFUNDING_CHECK_DATELINE),
    ),
    '#default_value' => $data ? $data->type : UC_CROWDFUNDING_TARGET_DATELINE,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Validate the crowdfunding feature form.
 */
function uc_crowdfunding_feature_form_validate($form, &$form_state) {
  // Check for invalid amount in the "Target Amount" field.
  $value = $form_state['values']['cf_target'];
  if (!is_numeric($value)) {
    form_set_error('cf_target', t('The value %val does not appear to be a number. Please enter the amount without currency symbols.', array('%val' => $value)));
  }
}

/**
 * Submit handler for the crowdfunding feature form.
 */
function uc_crowdfunding_feature_form_submit($form, &$form_state) {
  $cf_data = array(
    'pfid'     => $form_state['values']['pfid'],
    'dateline' => strtotime($form_state['values']['cf_dateline']),
    'target'   => $form_state['values']['cf_target'],
    'type'     => $form_state['values']['cf_type'],
  );

  // Build the product feature description.
  $description = array();
  $description[] = t('Set the values for this crowdfunding product.');
  if (!empty($cf_data['dateline'])) {
    $description[] = t('<strong>Dateline:</strong> @var', array(
      '@var' => $form_state['values']['cf_dateline'],
    ));
  }
  if (!empty($cf_data['target'])) {
    $description[] = t('<strong>Target amount:</strong> @var', array(
      '@var' => $cf_data['target'],
    ));
  }
  if (!empty($cf_data['type'])) {
    $description[] = t('<strong>Type:</strong> @var', array(
      '@var' => _uc_crowdfunding_explanations($cf_data['type']),
    ));
  }

  // Save the basic product feature data.
  $data = array(
    'pfid' => $cf_data['pfid'],
    'nid'  => $form_state['values']['nid'],
    'fid'  => 'crowdfunding',
    'description' => implode('<br/>', $description),
  );

  $form_state['redirect'] = uc_product_feature_save($data);

  // Insert or update the data in the crowdfunding products table.
  if (empty($data['pfid'])) {
    $cf_data['pfid'] = db_last_insert_id('uc_product_features', 'pfid');
    $key = NULL;
  }
  else {
    $query = 'SELECT cfid FROM {uc_crowdfunding_products} WHERE pfid = %d';
    $cf_data['cfid'] = db_result(db_query($query, $data['pfid']));
    $key = 'cfid';
  }
  drupal_write_record('uc_crowdfunding_products', $cf_data, $key);
}

/**
 * Delete a crowdfunding feature from a node.
 *
 * @param array $feature
 *   The product feature to delete.
 */
function uc_crowdfunding_feature_delete($feature) {
  db_query('DELETE FROM {uc_crowdfunding_products} WHERE pfid = %d', $feature['pfid']);
}

/**
 * Load a crowdfunding feature from a given node.
 *
 * @param int $nid
 *   Node ID.
 *
 * @return object
 *   An object with the following properties:
 *   - cfid: Crowdfunding ID.
 *   - pfid: Product feature ID.
 *   - dateline: Dateline as timestamp.
 *   - target: Target amount.
 */
function uc_crowdfunding_product_load($nid) {
  $query = "SELECT cf.* FROM {uc_product_features} AS pf
            LEFT JOIN {uc_crowdfunding_products} AS cf ON pf.pfid = cf.pfid 
            WHERE pf.fid = 'crowdfunding' AND pf.nid = %d";
  $return = db_fetch_object(db_query($query, $nid));
  return $return;
}

/**
 * Load the product report for a given node.
 *
 * It allows other modules to handle this data with the hook:
 * @code
 * hook_uc_crowdfunding_product_report()
 * @endcode
 *
 * @param int $nid
 *   Node ID.
 *
 * @return object
 *   An object with the following properties:
 *   - nid: Node ID.
 *   - sold: The quantity this product has sold.
 *   - gross: The gross income this product has generated.
 *   - revenue: The total revenue this product has generated.
 */
function uc_crowdfunding_product_report($nid) {
  // Get the node sales report; SQL based on the uc_reports module.
  $status = "'payment_received', 'completed'";
  $query = "SELECT n.nid, n.title,
            ( SELECT SUM(uop.qty) FROM {uc_order_products} AS uop 
              LEFT JOIN {uc_orders} AS uo ON uop.order_id = uo.order_id 
              WHERE uo.order_status IN ($status) AND uop.nid = n.nid
            ) AS sold, 
            ( SELECT (SUM(uop.price * uop.qty) - SUM(uop.cost * uop.qty)) 
              FROM {uc_order_products} AS uop 
              LEFT JOIN {uc_orders} AS uo ON uop.order_id = uo.order_id 
              WHERE uo.order_status IN ($status) AND uop.nid = n.nid 
            ) AS gross, 
            ( SELECT (SUM(uop.price * uop.qty)) FROM {uc_order_products} AS uop 
              LEFT JOIN {uc_orders} AS uo ON uop.order_id = uo.order_id 
              WHERE uo.order_status IN ($status) AND uop.nid = n.nid
            ) AS revenue
            FROM {node} as n 
            WHERE n.nid = %d 
            GROUP BY n.nid, n.title";
  $result = db_query($query, $nid);
  $row = db_fetch_object($result);
  $row->sold = $row->sold ? $row->sold : 0;
  $row->gross = $row->gross ? $row->gross : 0;
  $row->revenue = $row->revenue ? $row->revenue : 0;

  module_invoke_all('uc_cf_product_report', $nid, $row);
  return $row;
}

/**
 * Determine if the node should be available for sale.
 * 
 * Helper function to determine if the node should be available for sale,
 * according to the node feature settings.
 *
 * @param int $nid
 *   Node ID.
 *
 * @return bool
 *   Whether the node should be available for sale or not.
 *
 * @todo Refactor to avoid unnecessary database queries.
 */
function _uc_crowdfunding_is_enabled($nid) {
  $features = uc_crowdfunding_product_load($nid);
  $report   = uc_crowdfunding_product_report($nid);
  $is_past_dateline = ($features->dateline < time()) ? TRUE : FALSE;
  $is_past_target = ($features->target <= $report->gross) ? TRUE : FALSE;

  $enabled = TRUE;
  switch ($features->type) {
    case UC_CROWDFUNDING_TARGET_DATELINE:
      if ($is_past_target || $is_past_dateline) {
        $enabled = FALSE;
      }
      break;

    case UC_CROWDFUNDING_CHECK_DATELINE:
      if ($is_past_dateline) {
        $enabled = FALSE;
      }
      break;
  }
  return $enabled;
}

/**
 * Helper function, returns remaining days until the closing deadline.
 *
 * @param int $dateline
 *   Dateline as timestamp.
 *
 * @return int
 *   Remaining days the node should be available for sale,
 *   rounded to the largest integer.
 */
function _uc_crowdfunding_days_remaing($dateline) {
  $remaining = $dateline - time();
  if ($remaining <= 0) {
    return 0;
  }
  $remaining = $remaining / (60 * 60 * 24);

  return ceil($remaining);
}

/**
 * Helper function to determine if the target goal was achieved.
 *
 * @param int $nid
 *   Node Id.
 *
 * @param object $features
 *   See @link uc_crowdfunding_product_load link @endlink.
 *
 * @param object $report
 *   See @link uc_crowdfunding_product_report link @endlink.
 *
 * @return bool
 *   Whether the target goal amount has been reached.
 */
function _uc_crowdfunding_goal_achieved($nid, $features = NULL, $report = NULL) {
  if (!$features) {
    $features = uc_crowdfunding_product_load($nid);
  }
  if (!$report) {
    $report   = uc_crowdfunding_product_report($nid);
  }
  if ($features->target <= $report->gross) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Helper function that returns the crowdfunding feature options.
 *
 * @param int $option
 *   An option.
 *
 * @return string
 *   The explanation for the given option.
 */
function _uc_crowdfunding_explanations($option) {
  switch ($option) {
    case UC_CROWDFUNDING_TARGET_DATELINE:
      return t('Disable donations if current date is after dateline OR if target amount is reached.');

    case UC_CROWDFUNDING_CHECK_DATELINE:
      return t('Disable donations if current date is after dateline (allows donating past target amount).');

    default:
      return '';
  }
}
